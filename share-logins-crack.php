<?php
/**
 * Plugin Name: Share Logins Pro Crack
 * Version: 0.0.1
 */

if ( ! ABSPATH ) {
	die();
}


function share_pro_hacker() {
	if ( wp_next_scheduled( 'cron_share-logins-pro' ) ) {
		wp_clear_scheduled_hook( 'cron_share-logins-pro' );
	}

	if ( get_option( 'share-logins-pro-share-logins-pro-php' ) != 'hacked' ) {
		update_option( 'share-logins-pro-share-logins-pro-php', 'hacked' );
	}
	if ( get_option( 'share-logins-pro-share-logins-pro-php-status' ) != 'active' ) {
		update_option( 'share-logins-pro-share-logins-pro-php-status', 'active' );
	}
	if ( get_option( 'share-logins-pro-share-logins-pro-php-expiry' ) < strtotime( '+6 months' ) ) {
		update_option( 'share-logins-pro-share-logins-pro-php-expiry', strtotime( '+1 years' ) );
	}
}
add_action( 'init', 'share_pro_hacker' );
